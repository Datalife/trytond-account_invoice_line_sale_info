# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.tests.test_tryton import ModuleTestCase


class AccountInvoiceLineSaleInfoTestCase(ModuleTestCase):
    """Test Account Invoice Line Sale Info module"""
    module = 'account_invoice_line_sale_info'


del ModuleTestCase
